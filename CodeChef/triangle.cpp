#include <bits/stdc++.h>
using namespace std;
#define loop(i, a, n) for (auto i = a; i < n; i++)

int solve()
{
    int L, k, ans = 0;
    cin >> L >> k;

    return ans;
}
// given a triangle with L side and L+1 points on the side
// find the minimum number of triangles that can be formed of length k!
int main()
{
    int t;
    cin >> t;
    loop(i, 0, t) cout << "Case" << i + 1 << " " << solve() << endl;
    return 0;
}